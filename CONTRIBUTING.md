# How to contribute

Clone this repo:
````bash
git clone git@gitlab.com:carcheky/drupalgrumphp.git
````

Add to your composer:
````json
"repositories": [
    {
        "name": "carcheky/drupalgrumphp",
        "type": "path",
        "url": "private-files/drupalgrumphp",
        "options": {
            "symlink": true
        }
    }
]
````

Run in shell:
````bash
composer require  carcheky/drupalgrumphp --dev
````
